package kk.myapplication3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn;
    private EditText passWord;
    private EditText userName;
    private String mainUser = "anuruth";
    private String mainPass = "12345";
    private String username,password;
    private TextView createActText;

    private static final int REQUEST_CODE_CREATEACC = 11100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindView();
        initView();

        userName.setText("anuruth");
        passWord.setText("12345");

    }

    private void bindView()
    {
        userName = findViewById(R.id.loginUsername);
        passWord = findViewById(R.id.loginPassword);
        btn = findViewById(R.id.loginButton);
        createActText = findViewById(R.id.createActText);
        }

    public void initView(){
        username = userName.getText().toString();
        password = passWord.getText().toString();
        btn.setOnClickListener(this);

        createActText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getApplicationContext(),page2));
            }
        });
    }

    private boolean isStringCompare(String input,String compare){
        return input.equals(compare);
    }



    @Override
    public void onClick(View view) {

        if(isStringCompare(userName.getText().toString(),mainUser)&& isStringCompare(passWord.getText().toString(),mainPass)){
            Toast.makeText(MainActivity.this,"Login Succes",Toast.LENGTH_SHORT).show();
            sendMessage();

        }else {
            Toast.makeText(MainActivity.this,"Login Failed",Toast.LENGTH_SHORT).show();
            passWord.setText("");

        }

    }

    public void sendMessage() {
        Intent i = new Intent(getApplicationContext(),page2.class);
        i.putExtra("data",userName.getText().toString());
        startActivity(i);
    }

}


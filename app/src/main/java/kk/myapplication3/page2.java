package kk.myapplication3;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;

import static kk.myapplication3.R.id.txtCountry;


public class page2 extends AppCompatActivity implements View.OnClickListener {
    //    what is this
    public static final int REQUEST_CODE_GETCOUNTRY = 1040;
    public static final int REQUEST_CODE_GETAGE = 1050;
    //    -------
    private TextView txtdataUser,txtCountry;
    private EditText txtFullname, txtAge, txtBirthday;
    private Button btnAge, btnCountry, btnSubmit;
    private String getMessage, dataUser, fullname, birthday, country;
    private int age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page2);
        bindView();
        getMessage();
//        initView();
        setupCountryButton();
        setupBirthdayButton();

    }


    private void bindView() {
        txtdataUser = findViewById(R.id.txtUsername);
        txtFullname = findViewById(R.id.txtFullname);
        txtAge = findViewById(R.id.txtAge);
        txtBirthday = findViewById(R.id.txtDate);
        txtCountry = findViewById(R.id.txtCountry);
        btnAge = findViewById(R.id.btnAge);
        btnCountry = findViewById(R.id.btnCountry);
        btnSubmit = findViewById(R.id.btnSubmit);

    }

    private void initView() {
//        fullname = txtFullname.getText().toString();
//        age = Integer.parseInt(txtAge.getText().toString());
//        birthday = txtBirthday.getText().toString();
//        country = txtCountry.getText().toString();
//        btnSubmit.setOnClickListener(this);


    }

    public void setupCountryButton() {
        btnCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), page3_2.class);
//                startActivity(i);
                startActivityForResult(i, REQUEST_CODE_GETCOUNTRY);

            }
        });
    }


    private void setupBirthdayButton() {
        final Calendar c = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                c.set(Calendar.YEAR, year);
                c.set(Calendar.MONTH, month);
                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                birthday = DateFormat.getDateInstance().format(c.getTime());
                txtBirthday.setText(birthday);

            }
        };
        btnAge.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                new DatePickerDialog(page2.this, d, c.get(Calendar.YEAR), c.get(Calendar.MONTH),c.get(Calendar.DAY_OF_MONTH)).show();

//                DatePickFragment newFragment = new DatePickFragment();
//                newFragment.show(getFragmentManager(), "datePicker");

            }
        });
    }

    public void getMessage() {
        Bundle extras = getIntent().getExtras();
//        extras.clear();
        txtdataUser.setText(extras.getString("data", "User"));


    }

    @Override
    public void onClick(View v) {

    }
// Getting result from another page.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_GETCOUNTRY:
                if (resultCode == Activity.RESULT_OK) {
//                    get the message
                    String message = data.getStringExtra("ketCountryResult");
//                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
//                    do sth with it

                    txtCountry.setText(message);
                    txtCountry.onSaveInstanceState();
                    Toast.makeText(getApplicationContext(), "sve", Toast.LENGTH_SHORT).show();

                }

        }
    }
}




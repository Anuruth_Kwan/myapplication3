package kk.myapplication3;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class page3 extends AppCompatActivity {

    private Button btnThailand,btnSingapore,btnUSA,btnChina;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page3);

        bindView();
        setupThailandButton();
        setupSingaporeButton();
        setupUSAButton();
        setupChinaButton();
    }

    private void setupThailandButton() {
        btnThailand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra("ketCountryResult","Thailand");
                setResult(Activity.RESULT_OK,i);
                finish();
            }
        });

    }

    private void setupUSAButton() {
        btnUSA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra("ketCountryResult","USA");
                setResult(Activity.RESULT_OK,i);
                finish();
            }
        });

    }

    private void setupSingaporeButton() {
        btnSingapore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra("ketCountryResult","Singapore");
                setResult(Activity.RESULT_OK,i);
                finish();
            }
        });

    }

    private void setupChinaButton() {
        btnChina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra("ketCountryResult","China");
                setResult(Activity.RESULT_OK,i);
                finish();
            }
        });

    }

    public void bindView(){
        btnThailand = findViewById(R.id.btnThai);
        btnSingapore = findViewById(R.id.btnSingapore);
        btnUSA = findViewById(R.id.btnUSA);
        btnChina = findViewById(R.id.btnChina);
    }
}

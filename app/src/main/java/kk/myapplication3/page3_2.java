package kk.myapplication3;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class page3_2 extends AppCompatActivity {

    private ListView countries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page3_2);

        bindView();
        setupCountries();

        countries.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i  = new Intent();
                String s = countries.getItemAtPosition(position).toString();
                i.putExtra("ketCountryResult",""+s);
                setResult(Activity.RESULT_OK,i);
                finish();

//                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void bindView() {
        countries = findViewById(R.id.ListViewCountry);
    }

    public void setupCountries(){
        Locale[] locale = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        String country;
        for( Locale loc : locale ){
            country = loc.getDisplayCountry();
            if( country.length() > 0 && !countries.contains(country) ){
                countries.add( country );
            }
        }
        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, countries);
        this.countries.setAdapter(adapter);
    }





}
